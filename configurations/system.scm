;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024-2025 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>

(use-modules (gnu bootloader)
             (gnu bootloader grub)
             (gnu packages)
             (gnu services base)
             (gnu services databases)
             (gnu services desktop)
             (gnu services dns)
             (gnu services networking)
             (gnu services ssh)
             (gnu system file-systems)
             (gnu system keyboard)
             (gnu system locale)
             (guix channels)
             (guix gexp)
             (guix inferior)
             (guix packages)
             (guix transformations)
             (srfi srfi-1))

(define openfortivpn
  ((options->transformation '((with-version . "openfortivpn=1.21.0")))
   (specification->package "openfortivpn")))

(operating-system
  (kernel (specification->package "linux"))
  (kernel-arguments
    (append %default-kernel-arguments
     '("loglevel=0"
       "splash")))
  (firmware (list (specification->package "linux-firmware")))
  (file-systems
    (append %base-file-systems
      (list
        (file-system
          (mount-point "/boot/efi")
          (device (uuid "F206-C40F" 'fat))
          (type "vfat"))
        (file-system
          (mount-point "/")
          (device (uuid "f566bbb7-a718-4498-bb7f-006ae0425186" 'ext4))
          (type "ext4")))))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))))
  (timezone "Europe/Vilnius")
  (locale "lt_LT.utf8")
  (locale-definitions
    (append %default-locale-definitions
      (list
        (locale-definition
          (name "lt_LT.utf8")
          (source "lt_LT")
          (charset "UTF-8")))))
  (keyboard-layout (keyboard-layout "us"))
  (host-name "guix")
  (users
    (append %base-user-accounts
      (list
        (user-account
          (uid 0)
          (name "root")
          (group "root")
          (home-directory "/home/root")
          (shell (file-append (specification->package "bash") "/bin/bash")))
        (user-account
          (name "eidmar")
          (group "eidmar")
          (supplementary-groups '("wheel" "kvm"))
          (home-directory "/home/eidmar")
          (shell (file-append (specification->package "bash") "/bin/bash"))))))
  (groups
    (append %base-groups
      (list
        (user-group
          (name "eidmar")))))
  (skeletons '())
  (packages
    (append (list openfortivpn)
      (map (compose list specification->package+output)
        '("qutebrowser"
          "ungoogled-chromium"
          "nyxt"
          "google-chrome-stable"

          "ublock-origin-chromium"

          "dconf-editor"
          "drawing"
          "evolution"
          "gimp"
          "gitg"
          "glade"
          "gnome-boxes"
          "meld"
          "sysprof"
          "polari"
          "element-desktop"
          "gfeeds"

          "libreoffice"
          "nsxiv"
          "telegram-desktop"
          "transmission:gui"
          "vscodium"
          "obs"
          "celluloid"
          "signal-desktop"
          "kdenlive"

          "emacs-pgtk"
          "emacs-consult"
          "emacs-ement"
          "emacs-magit"
          "emacs-marginalia"
          "emacs-multiple-cursors"
          "emacs-orderless"
          "emacs-subed"
          "emacs-vertico"
          "emacs-which-key"
          "emacs-unfill"
          "emacs-go-mode"
          "emacs-jinx"

          "gnome-shell-extension-vitals"

          "r"
          "bash"
          "carp"
          "clojure"
          "dotnet"
          "fennel"
          "gforth"
          "ghc"
          "guile"
          "janet"
          "loko-scheme"
          "nasm"
          "node"
          "ocaml"
          "python"
          "sbcl"
          "octave"
          "smalltalk"
          "openjdk@20:jdk"
          "racket"
          "gcc-toolchain"
          "go"

          "e2fsprogs"
          "exfatprogs"
          "dosfstools"
          "ntfs-3g"
          "mergerfs"
          "httpfs2"
          "jmtpfs"

          "unrar"
          "unzip"
          "gzip"
          "p7zip"
          "tar"
          "innoextract"
          "xz"
          "wimlib"

          "coreutils"
          "make"
          "file"
          "findutils"
          "grep"
          "ripgrep"
          "sed"
          "gawk"
          "moreutils"
          "util-linux"
          "which"
          "xxd"
          "tree"
          "xdg-utils"
          "diffutils"

          "iproute2"
          "iputils"
          "inetutils"

          "pciutils"
          "hddtemp"
          "lshw"

          "git"
          "git:send-email"
          "git-annex"

          "curl"
          "wget"
          "yt-dlp"
          "aria2"
          "lgogdownloader"

          "mariadb"
          "patchelf"
          "wine64"
          "stapler"
          "alsa-utils"
          "isc-dhcp"
          "fdupes"
          "inxi"
          "rsync"
          "acpid"
          "reuse"
          "imagemagick"
          "python-adblock"
          "mandoc"
          "openssh"
          "omnisharp"
          "hexyl"
          "kmod"
          "procps"
          "gnupg"
          "graphviz"
          "pwgen"
          "drawterm"
          "shellcheck"
          "ffmpeg"
          "tokei"
          "rc"
          "jq"
          "jc"
          "entr"
          "texlive"
          "nss-certs"
          "recutils"
          "duperemove"
          "pandoc"
          "bubblewrap"
          "eyed3"
          "ncurses"
          "djvu2pdf"
          "skribilo"
          "qemu"
          "tesseract-ocr"
          "tesseract-ocr-tessdata-fast"
          "aspell"
          "aspell-dict-en"
          "aspell-dict-fr"
          "aspell-dict-lt"

          "steam"

          "font-gnu-unifont"))))
  (services
    (append
      (modify-services %desktop-services
        (delete guix-service-type)
        (delete network-manager-service-type)
        (delete special-files-service-type))
      (list
        ;;(service openssh-service-type
        ;;  (openssh-configuration
        ;;    (permit-root-login #t)))
        (service special-files-service-type
          `(("/bin/guile" ,(file-append (specification->package "guile") "/bin/guile"))
            ("/bin/bash" ,(file-append (specification->package "bash") "/bin/bash"))
            ("/bin/env" ,(file-append (specification->package "coreutils") "/bin/env"))
            ("/bin/sh" ,(file-append (specification->package "bash") "/bin/sh"))))
        (service guix-service-type
          (guix-configuration
            (substitute-urls
              (append %default-substitute-urls
                (list "https://substitutes.nonguix.org" "https://guix.bordeaux.inria.fr")))
            (authorized-keys
              (append %default-authorized-guix-keys
                (list
                  (plain-file "substitutes.nonguix.org.pub"
                    (string-append
                      "(public-key"
                      " (ecc"
                      "  (curve Ed25519)"
                      "  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
                  (plain-file "guix.bordeaux.inria.fr.pub"
                    (string-append
                      "(public-key"
                      " (ecc"
                      "  (curve Ed25519)"
                      "  (q #89FBA276A976A8DE2A69774771A92C8C879E0F24614AAAAE23119608707B3F06#)))")))))))
        (service gnome-desktop-service-type)
        (service network-manager-service-type
          (network-manager-configuration
            (dns "none")))
        (service dnsmasq-service-type
          (dnsmasq-configuration
            (cache-size 0)
            (no-resolv? #t)
            (query-servers-in-order? #t)
            (servers '("1.1.1.2"))))
        (service mysql-service-type)
        (simple-service 'resolv-service etc-service-type
          `(("resolv.conf" ,(plain-file "resolv.conf" "nameserver=127.0.0.1"))))))))
