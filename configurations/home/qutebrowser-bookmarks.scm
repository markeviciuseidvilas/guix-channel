;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024-2025 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>

(use-modules (ice-9 popen)
             (ice-9 receive)
             (ice-9 string-fun)
             (ice-9 textual-ports)
             (json)
             (rnrs bytevectors)
             (srfi srfi-1)
             (web client))

(define %confidential-data
  (call-with-input-file (string-append %current-directory "/home/confidential-data.scm")
    read))

(define (origin-paths+titles->bookmarks origin origin-title paths+titles)
  (append (list (string-join (list origin origin-title)))
    (map (lambda (path+title)
           (let ((path (car path+title))
                 (title (cadr path+title)))
             (string-append origin "/" path " " origin-title " / " title)))
      paths+titles)))

(string-join
  (append
    '("http://edwardfeser.com"
      "http://kirtis.info"
      "http://litlogos.eu"
      "http://paulgraham.com"
      "http://reconquistapress.com"
      "http://satenai.lt")

    '("https://4chan.org"
      "https://4channel.org"
      "https://4plebs.org"
      "https://9front.org"
      "https://aha-music.com"
      "https://aidai.eu"
      "https://aidas.lt"
      "https://alkas.lt"
      "https://alkonas.lt"
      "https://altcensored.com"
      "https://alytauskazimieras.lt"
      "https://amazon.com"
      "https://angeluspress.org"
      "https://annas-archive.org"
      "https://antelopehillpublishing.com"
      "https://app.element.io"
      "https://app.kits.ai"
      "https://aquinas.cc"
      "https://arxiv.org"
      "https://ateitieszurnalas.lt"
      "https://ateitis.lt"
      "https://atlassian.net"
      "https://atodangos.com"
      "https://austsaule.lv"
      "https://based.cooking"
      "https://bernardinai.lt"
      "https://bibleref.com"
      "https://biblija.lt"
      "https://bisqwit.iki.fi"
      "https://bitsearch.to"
      "https://britishpathe.com"
      "https://buciumul.ro"
      "https://cat-v.org"
      "https://catenabible.com"
      "https://catholic.com"
      "https://catholicfidelity.com"
      "https://catholiclibrary.org"
      "https://ccel.org"
      "https://chat.openai.com"
      "https://chire.fr"
      "https://christianbwagner.com"
      "https://ciurlionis.licejus.lt"
      "https://clanneireann.ie"
      "https://classics.mit.edu"
      "https://codeberg.org"
      "https://comunitateaidentitara.com"
      "https://counter-currents.com"
      "https://cozyclips.net"
      "https://ctmucommunity.org"
      "https://cutsinger.net"
      "https://cygwin.com"
      "https://deepai.org"
      "https://deepl.com"
      "https://desuarchive.org"
      "https://dexonline.ro"
      "https://dictionar-urban.ro"
      "https://discogs.com"
      "https://discord.com"
      "https://divinumofficium.com"
      "https://dominospizza.lt"
      "https://duckduckgo.com"
      "https://duomenubaze.lkc.lt"
      "https://e-kinas.lt"
      "https://ekalba.lt"
      "https://elevenlabs.io"
      "https://esveikata.lt"
      "https://fericiticeiprigoniti.net"
      "https://flashcardo.com"
      "https://fmhy.pages.dev"
      "https://forum.osdev.org"
      "https://forvo.com"
      "https://freelistenonline.com"
      "https://fsf.org"
      "https://fsspx.news"
      "https://fsspx.org"
      "https://game-icons.net"
      "https://gizmo.ai"
      "https://gmail.com"
      "https://gobolinux.org"
      "https://gog.com"
      "https://gornahoor.net"
      "https://gothicarchive.org"
      "https://gutenberg.org"
      "https://haiku-os.org"
      "https://haydockcommentary.com"
      "https://holtz.org"
      "https://humanknowledge.net"
      "https://iep.utm.edu"
      "https://imginn.com"
      "https://incorectpolitic.com"
      "https://john-uebersax.com"
      "https://jonathanbowden.org"
      "https://kaisiadoriuvyskupija.lt"
      "https://kalbi.lt"
      "https://karmelituparapija.lt"
      "https://katbib.lt"
      "https://katekizmas.lt"
      "https://kaunoarkivyskupija.lt"
      "https://kki.lt"
      "https://krikscioniuprofsajunga.lt"
      "https://kristoteka.lt"
      "https://kronikimyrtany.pl"
      "https://kuriulietuvai.lt"
      "https://laikmetis.lt"
      "https://lainchan.org"
      "https://laiskailietuviams.lt"
      "https://laisvavisuomene.lt"
      "https://last.fm"
      "https://legiunea.com"
      "https://libgen.is"
      "https://libgen.rs"
      "https://libgen.st"
      "https://lietuvai.lt"
      "https://limis.lt"
      "https://lituanistika.lt"
      "https://lkma.lt"
      "https://llvs.lt"
      "https://lrs.lt"
      "https://lrt.lt"
      "https://lumalabs.ai"
      "https://lvk.lcn.lt"
      "https://lyricstranslate.com"
      "https://maceina.lt"
      "https://maldos.lt"
      "https://malduknyga.lt"
      "https://mally.stanford.edu"
      "https://maphub.net"
      "https://massgrave.dev"
      "https://maypoleofwisdom.com"
      "https://mega.nz"
      "https://melobytes.com"
      "https://melskis.lt"
      "https://messenger.com"
      "https://metaphysicist.com"
      "https://militia-immaculatae.info"
      "https://militia-immaculatae.org"
      "https://miscarea.net/w"
      "https://modarchive.org"
      "https://morsecode.world"
      "https://nemunoaušra.lt"
      "https://newadvent.org"
      "https://newmanreader.org"
      "https://nonguix.org"
      "https://notrelated.xyz"
      "https://nyaa.si"
      "https://ocw.mit.edu"
      "https://office.com"
      "https://ogoranu.ro"
      "https://on.lt"
      "https://online.lt"
      "https://openlibrary.org"
      "https://opensubtitles.org"
      "https://outlook.com"
      "https://pakartot.lt"
      "https://paneveziovyskupija.lt"
      "https://partizanai.org"
      "https://patch-shop.com"
      "https://peticijos.lt"
      "https://pildyk.lt"
      "https://pinesap.net"
      "https://pipedija.lt"
      "https://pixlr.com"
      "https://plato-dialogues.org"
      "https://plato.stanford.edu"
      "https://poetii-nostri.ro"
      "https://polcompball.net"
      "https://prodeoetpatria.lt"
      "https://project-mage.org"
      "https://propatria.lt"
      "https://qwant.com"
      "https://rateyourmusic.com"
      "https://raštija.lt"
      "https://redketchup.io"
      "https://remove.bg"
      "https://resee.it"
      "https://returntotheland.org"
      "https://rofondas.lt"
      "https://romanianvoice.com"
      "https://rozancius.lt"
      "https://rutracker.org"
      "https://sacred-texts.com"
      "https://sanctamissa.org"
      "https://savitridevi.org"
      "https://sb.lt"
      "https://sci-hub.ru"
      "https://sci-hub.se"
      "https://sci-hub.st"
      "https://sena.lt"
      "https://serenityos.org"
      "https://shanson-e.tk"
      "https://sheldrake.org"
      "https://siauliuvyskupija.lt"
      "https://smetona.lt"
      "https://smnz.de"
      "https://sr.ht"
      "https://sspx.org"
      "https://stonetoss.com"
      "https://stormfront.org"
      "https://suno.ai"
      "https://susivienijimas.lt"
      "https://swedbank.lt"
      "https://t2bot.io"
      "https://tautosakos-rankrastynas.lt"
      "https://telsiuvyskupija.lt"
      "https://temp-mailbox.com"
      "https://templeos.net"
      "https://templeos.org"
      "https://textboard.org"
      "https://thecenterforsophiologicalstudies.com"
      "https://thomisticmetaphysics.com"
      "https://tineye.com"
      "https://tradere.lt"
      "https://translate.fedoraproject.org"
      "https://truerestoration.org"
      "https://udio.com"
      "https://urbandictionary.com"
      "https://valstybingumas.com"
      "https://versme.lt"
      "https://vidzgiriobaznycia.lt"
      "https://viggle.ai"
      "https://vilkaviskiovyskupija.lt"
      "https://vilnensis.lt"
      "https://vle.lt"
      "https://vocalremover.org"
      "https://vydija.lt"
      "https://vydunodraugija.lt"
      "https://warosu.org"
      "https://web.postman.co"
      "https://wiby.me"
      "https://wiby.org"
      "https://wiki.c2.com"
      "https://wiki.osdev.org"
      "https://wikiart.org"
      "https://wikitree.com"
      "https://wolframalpha.com"
      "https://www-formal.stanford.edu"
      "https://www.marijosradijas.lt"
      "https://www.vatican.va"
      "https://www.wga.hu"
      "https://zeitgeistreviews.com"
      "https://zie87.github.io"
      "https://šaltiniai.info")

    (origin-paths+titles->bookmarks "https://archive.org" "Internet Archive"
      '(("details/@kataliki_ka_biblioteka" "Profiliai / Katalikų biblioteka")
        ("details/@markeviciuseidvilas" "Profiliai / Eidvilas")))

    (map string-join
      '(("https://archyvai.lt" "Lietuvos vyriausiojo archyvaro tarnyba")
        ("https://epartizanai.archyvai.lt" "E-Partizanų archyvas")
        ("https://lyavaizdai.archyvai.lt" "LYA E-Vaizdų archyvas")))

    (map string-join
      '(("https://arhanghelul.org" "Arhanghelul")
        ("https://arhiva.arhanghelul.org" "Arhanghelul / Arhivă")
        ("https://wiki.arhanghelul.org" "Arhanghelul / Wiki")))

    (map string-join
      '(("https://blogspot.com" "Blogger")
        ("https://bibliotecafascista.blogspot.com" "Blogger / Svetainės / Biblioteca Fascista")
        ("https://czc50.blogspot.com" "Blogger / Svetainės / Adevarul despre Corneliu Zelea Codreanu")
        ("https://don-colacho.blogspot.com" "Blogger / Svetainės / Don Colacho’s Aphorisms")
        ("https://edwardfeser.blogspot.com" "Blogger / Svetainės / Edward Feser")
        ("https://tautininkas.blogspot.com" "Blogger / Svetainės / Tomo Skorupskio blogas")
        ("https://thearmchairthomist.blogspot.com" "Blogger / Svetainės / The Armchair Thomist")
        ("https://thewaywardaxolotl.blogspot.com" "Blogger / Svetainės / The Wayward Axolotl")
        ("https://williamsonletters.blogspot.com" "Blogger / Svetainės / Bishop Williamson's Letters")
        ("https://yurasys.blogspot.com" "Blogger / Svetainės / Плейграунд Никиты")))

    (map string-join
      '(("https://christogenea.org" "Christogenea")
        ("https://saxonmessenger.christogenea.org" "Christogenea / The Saxon Messenger")))

    (origin-paths+titles->bookmarks "https://docsbot.ai" "DocsBot"
      '(("tools/image/text-extractor" "Įrankiai / Free AI Image to Text Extractor")))

    (origin-paths+titles->bookmarks "https://epaveldas.lt" "ePaveldas"
      '(("preview?id=C1B0003824101" "Karys")
        ("preview?id=LNB00446F96" "Akademikas")
        ("preview?id=LNB014856D2" "Jaunoji Lietuva")
        ("preview?id=LNB1F07AC29" "Jaunoji karta")))

    (origin-paths+titles->bookmarks "https://facebook.com" "Facebook"
      '(("?sk=h_chr" "Sklaidos kanalai")
        ("markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://fsspx.lt" "FSSPX Lietuva"
      '(("liturginis-kalendorius" "Liturginis kalendorius")
        ("maldos-ir-giesmes" "Maldos ir giesmės")
        ("pamaldu-tvarka-kaune" "Pamaldų tvarka Kaune")))

    (origin-paths+titles->bookmarks "https://github.com" "GitHub"
      '(("markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://gitlab.com" "GitLab"
      '(("markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (map string-join
      '(("https://gnome.org" "GNOME")
        ("https://gitlab.gnome.org" "GNOME / GitLab")
        ("https://l10n.gnome.org" "GNOME / Damned Lies")
        ("https://wiki.gnome.org" "GNOME / Wiki")))

    (map string-join
      '(("https://gnu.org" "GNU")
        ("https://guix.gnu.org" "GNU / Guix")
        ("https://guix.gnu.org/blog" "GNU / Guix / Blog")
        ("https://guix.gnu.org/manual/devel/en/guix.html" "GNU / Guix / Reference Manual")
        ("https://guix.gnu.org/manual/devel/en/guix.html#Building-from-Git" "GNU / Guix / Reference Manual / Building from Git")
        ("https://guix.gnu.org/manual/devel/en/guix.html#Updating-the-Guix-Package" "GNU / Guix / Reference Manual / Updating the Guix Package")
        ("https://issues.guix.gnu.org" "GNU / Guix / Issue Tracker")
        ("https://logs.guix.gnu.org" "GNU / Guix / IRC Channel Logs")
        ("https://packages.guix.gnu.org" "GNU / Guix / Package Browser")
        ("https://savannah.gnu.org" "GNU / Savannah")))

    (map string-join
      '(("https://google.com" "Google")
        ("https://drive.google.com" "Google Drive")
        ("https://fonts.google.com" "Google Fonts")
        ("https://gemini.google.com" "Google Gemini")
        ("https://groups.google.com" "Google Groups")
        ("https://images.google.com" "Google Images")
        ("https://mail.google.com" "Google Mail")
        ("https://maps.google.com" "Google Maps")
        ("https://scholar.google.com" "Google Scholar")
        ("https://translate.google.com" "Google Translate")))

    (origin-paths+titles->bookmarks "https://instagram.com" "Instagram"
      '(("eidvilas.markevicius" "Eidvilas Markevičius")))

    (map string-join
      '(("https://katalikai.lt" "Lietuvos Katalikų Bažnyčia")
        ("https://lk.katalikai.lt" "Lietuvos Katalikų Bažnyčia / Liturginis kalendorius")
        ("https://maldynas.katalikai.lt" "Lietuvos Katalikų Bažnyčia / Maldynas")
        ("https://paveldas.katalikai.lt" "Lietuvos Katalikų Bažnyčia / Lietuvos krikščioniškasis paveldas")
        ("https://vl.katalikai.lt" "Lietuvos Katalikų Bažnyčia / Valandų liturgija")))

    (origin-paths+titles->bookmarks "https://katalikutradicija.lt" "Katalikų Tradicija"
      '(("katekizmas" "Katekizmas")
        ("sv-misiu-tvarka" "Šv. Mišių tvarka")
        ("sventosios-misios/liturginiai-metai" "Liturginių metų šv. Mišios")))

    (map string-join
      '(("https://ktu.edu" "KTU")
        ("https://moodle.ktu.edu" "KTU / Moodle")))

    (map string-join
      '(("https://ktu.lt" "KTU")
        ("https://uais.cr.ktu.lt" "KTU / Akademinė informacinė sistema")))

    (map string-join
      '(("https://lukesmith.xyz" "Luke Smith")
        ("https://videos.lukesmith.xyz" "Luke Smith / PeerTube")))

    (map string-join
      '(("https://metapedia.org" "Metapedia")
        ("https://en.metapedia.org" "Metapedia")
        ("https://ro.metapedia.org" "Metapedia")))

    (origin-paths+titles->bookmarks "https://odysee.com" "Odysee"
      '(("@markeviciuseidvilas:3" "Kanalai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://pinterest.com" "Pinterest"
      '(("markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://reddit.com" "Reddit"
      '(("u/markeviciuseidvlias" "Reddit / Profiliai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://restream.io" "Restream"
      '(("tools/transcribe-audio-to-text" "Įrankiai / Transcribe Audio to Text")
        ("tools/transcribe-video-to-text" "Įrankiai / Transcribe Video to Text")))

    (map string-join
      '(("https://softwareheritage.org" "Software Heritage")
        ("https://archive.softwareheritage.org" "Software Heritage Archive")))

    (origin-paths+titles->bookmarks "https://soundcloud.com" "SoundCloud"
      '(("markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (origin-paths+titles->bookmarks "https://spauda.org" "Spauda.org"
      '(("vytis-laikrastis" "Vytis")))

    (map string-join
      '(("https://stackexchange.com" "Stack Exchange")
        ("https://philosophy.stackexchange.com" "Stack Exchange / Philosophy")))

    (map string-join
      '(("https://stephenwolfram.com" "Stephen Wolfram")
        ("https://writings.stephenwolfram.com" "Stephen Wolfram / Writings")))

    (map string-join
      '(("https://substack.com" "Substack")
        ("https://chrislangan.substack.com" "Substack / Svetainės / Chris Langan's Ultimate Reality")
        ("https://druidstaresback.substack.com" "Substack / Svetainės / The Druid Stares Back")
        ("https://ephemeralsolidity.substack.com" "Substack / Svetainės / Ephemeral’s Substack")
        ("https://fascio.substack.com" "Substack / Svetainės / The Fascio Newsletter")
        ("https://fleurinverse.substack.com" "Substack / Svetainės / Fleur’s Substack")
        ("https://imperiumpress.substack.com" "Substack / Svetainės / Imperium Press")
        ("https://keithwoodspub.substack.com" "Substack / Svetainės / Keith Woods")
        ("https://megafoundation.substack.com" "Substack / Svetainės / TELEOLOGIC: CTMU Teleologic Living")
        ("https://millennialwoes.substack.com" "Substack / Svetainės / Millennial Woes")
        ("https://reginastatkuvien.substack.com" "Substack / Svetainės / Regina Publication")
        ("https://richardthefourth.substack.com" "Substack / Svetainės / Richard The Fourth")
        ("https://robertaspetrauskas.substack.com" "Substack / Svetainės / Robertas Petrauskas")
        ("https://symmetria.substack.com" "Substack / Svetainės / A Play of Masks")
        ("https://wmreview.substack.com" "Substack / Svetainės / The WM Review")))

    (origin-paths+titles->bookmarks "https://tiktok.com" "TikTok"
      '(("@markeviciuseidvilas" "Profiliai / Eidvilas Markevičius")))

    (map string-join
      '(("https://vlkk.lt" "VLKK")
        ("https://terminai.vlkk.lt" "VLKK / Terminų bankas")))

    (map string-join
      '(("https://wikipedia.org" "Wikipedia")
        ("https://en.wikipedia.org" "Wikipedia")
        ("https://la.wikipedia.org" "Vicipaedia")
        ("https://lt.wikipedia.org" "Vikipedija")
        ("https://lv.wikipedia.org" "Vikipēdija")
        ("https://ro.wikipedia.org" "Wikipedia")))

    (map string-join
      '(("https://wikiquote.org" "Wikiquote")
        ("https://en.wikiquote.org" "Wikiquote")
        ("https://la.wikiquote.org" "Vicicitatio")
        ("https://lt.wikiquote.org" "Vikicitatos")
        ("https://lv.wikiquote.org" "Vikicitāti")
        ("https://ro.wikiquote.org" "Wikicitat")))

    (map string-join
      '(("https://wikisource.org" "Wikisource")
        ("https://en.wikisource.org" "Wikisource")
        ("https://la.wikisource.org" "Vicifons")
        ("https://lt.wikisource.org" "Vikišaltiniai")
        ("https://lv.wikisource.org" "Vikiavoti")
        ("https://ro.wikisource.org" "Wikisource")))

    (map string-join
      '(("https://wiktionary.org" "Wiktionary")
        ("https://en.wiktionary.org" "Wiktionary")
        ("https://la.wiktionary.org" "Victionarium")
        ("https://lt.wiktionary.org" "Vikižodynas")
        ("https://lv.wiktionary.org" "Vikivārdnīca")
        ("https://ro.wiktionary.org" "Wikționar")))

    (map string-join
      '(("https://wordpress.com" "WordPress")
        ("https://catholicgnosis.wordpress.com" "WordPress / Svetainės / Christian Platonism")
        ("https://godkingandnation.wordpress.com" "WordPress / Svetainės / God, King, and Nation")
        ("https://hughjidiette.wordpress.com" "WordPress / Svetainės / Hugh Jidiette")
        ("https://isabelavs2.wordpress.com" "WordPress / Svetainės / Isabela Vasiliu-Scraba")
        ("https://mathomablog.wordpress.com" "WordPress / Svetainės / Mathoma")
        ("https://praytherosaryeveryday.wordpress.com" "WordPress / Svetainės / The Daily Decade")
        ("https://theosymmetry.wordpress.com" "WordPress / Svetainės / Symmetria")))

    (origin-paths+titles->bookmarks "https://x.com" "X"
      '(("eidmar_" "Profiliai / Eidvilas Markevičius")))

    (map string-join
      '(("https://youtube.com" "YouTube")
        ("https://youtube.com/channel/UCjoBZHz5QXR23nWwy9Jw80Q" "YouTube / Kanalai / Eidvilas Markevičius")))

    (let loop ((page-token #f) (accumulated-bookmarks '()))
      (let ((request-url
              (string-append
                "https://www.googleapis.com/youtube/v3/subscriptions?channelId=UCjoBZHz5QXR23nWwy9Jw80Q"
                "&part=snippet"
                "&maxResults=50"
                "&key=" (assoc-ref %confidential-data "www.googleapis.com:api-keys:key1")
                (if page-token (string-append "&pageToken=" page-token) ""))))
        (receive (header body) (http-request request-url)
          (let* ((parsed-body (json-string->scm (utf8->string body)))
                 (next-page-token (assoc-ref parsed-body "nextPageToken"))
                 (items (vector->list (assoc-ref parsed-body "items")))
                 (bookmarks
                   (map (lambda (item)
                          (let* ((snippet (assoc-ref item "snippet"))
                                 (resource-id (assoc-ref snippet "resourceId"))
                                 (channel-id (assoc-ref resource-id "channelId"))
                                 (title (assoc-ref snippet "title")))
                            (string-join
                              (list (string-append "https://youtube.com/channel/" channel-id)
                                    (string-append "YouTube / Kanalai / " title)))))
                     items)))
            (if next-page-token
              (loop next-page-token (append accumulated-bookmarks bookmarks))
              (append accumulated-bookmarks bookmarks))))))

    (map string-join
      '(("https://youtube.com/feed/playlists" "YouTube / Grojaraščiai")
        ("https://youtube.com/playlist?list=LL" "YouTube / Grojaraščiai / Patikę vaizdo įrašai")
        ("https://youtube.com/playlist?list=WL" "YouTube / Grojaraščiai / Žiūrėti vėliau")))

    (string-split
      (string-replace-substring
        (get-string-all
          (open-input-pipe
            (string-join
              '("yt-dlp"
                "--skip-download"
                "--flat-playlist"
                "--print '%(url)s YouTube / Grojaraščiai / %(playlist_uploader)s / %(title)s'"
                "https://youtube.com/channel/UCjoBZHz5QXR23nWwy9Jw80Q/playlists"))))
        "https://www." "https://")
      #\newline)

    (map string-join
      '(("https://youtube.com/playlist?list=OLAK5uy_luPf5R780BhPsX46q5RZWL8oiOYWlc-CI" "YouTube / Grojaraščiai / Factory 81 / Factory 81")
        ("https://youtube.com/playlist?list=OLAK5uy_myCu4xFBpD2PPa4UUuZuLAnhnFRoaiWLQ" "YouTube / Grojaraščiai / Lacuna Coil / Comalies")
        ("https://youtube.com/playlist?list=OLAK5uy_nTuhXFhuSwJV4rdfkq5pyMgKy6XGCTpTs" "YouTube / Grojaraščiai / Adore / Children at Play")
        ("https://youtube.com/playlist?list=PLXdP7kGd8ek0utU8-9V7pLDP9fjC9Q0LK" "YouTube / Grojaraščiai / FSSPX Lietuva / Katechezės paskaitos: Pradžios knyga")
        ("https://youtube.com/playlist?list=PLXdP7kGd8ek3pjbD925ZLxDPDyBz9RI4t" "YouTube / Grojaraščiai / FSSPX Lietuva / Katechezės paskaitos: Dievo angelai")
        ("https://youtube.com/playlist?list=PLnftOVqh-jlaOmrPYw6TqwY4HAvT5rU_l" "YouTube / Grojaraščiai / Sensus Fidelium / Bible Study Class: Book of Genesis - St. Joan of Arc"))))
  "\n")
