;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2024 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>

(("gitlab.com:username" . "<...>")
 ("gitlab.com:password" . "<...>")

 ("github.com:username" . "<...>")
 ("github.com:personal-access-tokens:token1" . "<...>")

 ("gmail.com:password:git-send-email" . "<...>")

 ("gog.com:email" . "<...>")
 ("gog.com:password" . "<...>")

 ("www.googleapis.com:api-keys:key1" . "<...>"))
